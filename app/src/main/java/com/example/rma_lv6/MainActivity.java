package com.example.rma_lv6;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ImageClickListener {

    private EditText etStudentName;
    private Button btnAdd;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Initialize();
        setupRecyclerView();
    }

    private void Initialize() {
        etStudentName = (EditText)findViewById(R.id.etInputName);
        btnAdd = (Button)findViewById(R.id.btnAdd);
    }

    @Override
    public void OnImageClick(int position) {
        recyclerViewAdapter.removeStudent(position);
    }

    private void setupRecyclerView() {
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAdapter = new RecyclerViewAdapter(this);
        recyclerView.setAdapter(recyclerViewAdapter);
        List<String>data=new ArrayList<String>();
        recyclerViewAdapter.add(data);
    }

    public void onClickAdd(View view){
        String inputName = etStudentName.getText().toString();
            recyclerViewAdapter.addStudent(inputName);
    }
}