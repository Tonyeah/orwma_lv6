package com.example.rma_lv6;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private TextView tvStudentName;
    private ImageView ivImage;
    private ImageClickListener clickListener;

    public CustomViewHolder(@NonNull View itemView, ImageClickListener listener){
        super(itemView);
        tvStudentName =(TextView)itemView.findViewById(R.id.tvStudentName);
        ivImage=(ImageView)itemView.findViewById(R.id.ivImage);
        this.clickListener=listener;
        ivImage.setOnClickListener(this);
    }

    public void setName(String name){
        tvStudentName.setText(name);
    }

    @Override
    public void onClick(View v) {
        clickListener.OnImageClick(getAdapterPosition());
    }
}