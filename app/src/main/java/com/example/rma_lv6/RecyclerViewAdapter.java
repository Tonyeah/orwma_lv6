package com.example.rma_lv6;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private List<String> dataList = new ArrayList<>();
    private ImageClickListener listener;
    public static int counter = 0;

    public RecyclerViewAdapter(ImageClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View studentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_name, parent, false);
        return new CustomViewHolder(studentView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder viewHolder, int position) {
        viewHolder.setName(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void add(List<String>data){
        dataList.clear();
        dataList.addAll(data);
        notifyDataSetChanged();
    }

    public void addStudent(String name){
        if(name!=""){
            dataList.add(counter, name);
            notifyItemInserted(counter);
            counter+=1;
        }
    }
    public void removeStudent(int position){
        if(position>=0) {
            dataList.remove(position);
            notifyItemRemoved(position);
            counter -= 1;
        }
    }
}